import java.util.Scanner;

public class TestDoxy {

	private static double num1;
	private static double num2;
	static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {

		TestDoxy td = new TestDoxy();
		System.out.print("Do you want to [a]dd, [s]ubtract, [m]ultiply or [d]ivide? ");
		char choice = in.next().charAt(0);

		switch(choice) {
		case 'a':
			System.out.println("You have selected add.");
			td.prompt();
			System.out.println(num1 + " + " + num2 + " = " + td.sum(num1, num2));
			break;
		case 's':
			System.out.println("You have selected subtract.");
			td.prompt();
			System.out.println(num1 + " - " + num2 + " = " + td.subtract(num1, num2));
			break;
		case 'm':
			System.out.println("You have selected multiply.");
			td.prompt();
			System.out.println(num1 + " * " + num2 + " = " + td.multiply(num1, num2));
			break;
		case 'd':
			System.out.println("You have selected divide.");
			td.prompt();
			System.out.println(num1 + " / " + num2 + " = " + td.divide(num1, num2));
			break;
		default:
			System.out.println("Invalid input, bye bye!");
		}

	}

	double sum(double num1, double num2) {
		return num1 + num2;
	}

	double subtract(double num1, double num2) {
		return num1 - num2;
	}

	double multiply(double num1, double num2) {
		return num1 * num2;
	}

	double divide(double num1, double num2) {
		return num1 / num2;
	}

	void prompt() {
		System.out.print("Please enter the first number: ");
		num1 = in.nextDouble();
		System.out.print("Please enter the second number ");
		num2 = in.nextDouble();
	}

}
